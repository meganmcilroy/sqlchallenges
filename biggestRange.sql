--DROP PROCEDURE biggestRange
--GO
--CREATE PROCEDURE biggestRange
--@date varchar(12)
--AS
--SELECT [date],MAX([open]-[close]) 
--SUBSTRING (@selectedDate,1,8)AS date, 
--SUM ([open]-[close])AS range, 
--MAX ([high]) AS timeMaxPrice
--FROM sample_dataset3
--WHERE 
--GROUP BY [date]
--GO

--SELECT["Date"],max(High) as highPrice
--FROM sample_dataset3
--WHERE ["Date"] in ('06/08/2006','06/13/2006','06/14/2006')
--GROUP BY ["Date"];
DROP PROCEDURE biggestRange
GO
CREATE PROCEDURE biggestRange
AS
--getting the top 3 tables and range
SELECT DISTINCT TOP 3 ["Date"],abs(["Open"]-["Close"]) as Range
INTO #rangeTable
FROM sample_dataset3
ORDER BY Range DESC;

--max price for each date
SELECT DISTINCT TOP 3 ["Date"] as date,max(["High"]) as maxHigh
INTO #highPriceTable
FROM sample_dataset3
WHERE ["Date"] in (SELECT ["Date"] FROM #rangeTable)
GROUP BY ["Date"]
ORDER BY maxHigh ASC;

--get the time at which the high occurs for the dates
SELECT DISTINCT TOP 3 sample_dataset3.["Date"] as date, ["TIME"] as maxTime
INTO #highTimeTable
FROM sample_dataset3, #highPriceTable
WHERE sample_dataset3.["Date"] = #highPriceTable.Date and sample_dataset3.["High"] = #highPriceTable.maxHigh
--joining the 2 temp tables together
SELECT DISTINCT #rangeTable.["Date"] as Date, Range, maxTime as maxTime
FROM #rangeTable left join #highTimeTable
on #rangeTable.["Date"] = #highTimeTable.[date]
GO

exec biggestRange


