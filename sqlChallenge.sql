DROP PROCEDURE calculatingVolume
GO
CREATE PROCEDURE [dbo].[calculatingVolume]
@selectedDate varchar (12)
AS
	SELECT ticker,
	SUM(vol*[close])/SUM(vol) as volumeWeightedPrice, 
	substring(@selectedDate,7,2)+'/'+substring(@selectedDate,5,2)+'/'+substring(@selectedDate,1,4) as [date],
	'START: ('+substring(convert(varchar(12),convert(datetime, (substring(@selectedDate,1,8)+' '+substring(@selectedDate,9,2)+':'+substring(@selectedDate,11,2) )),108),1,5)+
	') - END: ('+substring(convert(varchar, dateAdd(hh,+5, 
		convert(datetime, 
		(substring(@selectedDate,1,8)+' '+
		substring(@selectedDate,9,2)+':'+substring(@selectedDate,11,2))
		)
		),108),1,5)+')' as interval
	FROM sd2
	WHERE convert(datetime, 
		(substring([date],1,8)+' '+
		substring([date],9,2)+':'+substring([date],11,2))) BETWEEN convert(datetime, 
		(substring(@selectedDate,1,8)+' '+
		substring(@selectedDate,9,2)+':'+substring(@selectedDate,11,2))) AND dateAdd(hh,+5, 
		convert(datetime, 
		(substring(@selectedDate,1,8)+' '+
		substring(@selectedDate,9,2)+':'+substring(@selectedDate,11,2))
		)
		)
	GROUP BY ticker
GO
EXEC calculatingVolume '201010110900'


