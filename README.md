#Field and name declarations
ticker - varchar - 4
date - varchar - 12
open - decimal - 8,4
high - decimal - 8,4
low - decimal - 8,4
close - decimal - 8,4
vol - varchar - 7

#Assumptions
- Trade price is the closing column. This is an assumption because this 
is the final transaction regardless of what happens throughout the day. 
- Volume price is the vol column. This is an assumption due to the 
likelihood that volumne=vol.
-Trading Symbol is ticker. This is an assumption as the first select 
is used to find information about the subject - in this case, the apple 
trade in row ticker.
